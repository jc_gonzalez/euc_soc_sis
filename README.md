Euclid SOC SIS Subsystem
==========================================

This directory and all its sub-directories contain the launcher and modules of the
Euclid SOC SIS Subsystem software project.

Installation and Execution
--------------------------

See the README file in the project `euc_soc_sis-deploy`.

License
--------

You should have received a copy of the GNU Lesser General Public License
along with QPF (please, see [COPYING][1] and [COPYING.LESSER][2] for
information on this license).  If not, see <http://www.gnu.org/licenses/>.

[1]: ./COPYING
[2]: ./COPYING.LESSER
