#!/bin/bash 
#------------------------------------------------------------------------------------
# SISController.sh
# Launcher for SIS Controller
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}

${PYTHON:=python3} $SCRIPTPATH/sis_controller/sis_controller.py $*
