#!/bin/bash
#------------------------------------------------------------------------------------
# le1_enhance.sh
# Enhance LE1 metadata
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2019-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}:${PYTHONATH}

#- Default values
TMPDIR=/tmp/$$.dir
TYPE=""
providedTYPE=false

ValidTypes="HKTM OP_PLAN OSS MIB SCI LE1 QLA"

usage () {
    echo ""
    echo "$0  [ -h ]  -t <prodType>  <input-files-or-folders> . . ."
    echo ""
    echo "where:"
    echo "  -h   Show this message"
    echo "  -t   Mandatory. Specifies the type of files we want to ingest into the EAS."
    echo "       Possible values are:"
    echo "         - SCI      : Raw VIS or NISP Science data files"
    echo "         - LE1      : LE1 VIS/NISP Products (Enhanced)"
    echo "         - QLA      : QLA Reports"
    echo "         - HKTM     : HK/TM Generated products"
    echo "         - ICR      : An Instrument Commanding Request"
    echo "         - SRV      : The Reference Survey"
    echo "         - OSS      : Operational Sky Survey"
    echo "         - OP_PLAN  : SSR-I and SSR/FD files"
    echo "         - MIB      : The Mission Data Base set of files"
    echo "         - OEM      : The Orbit file"
    echo "         - IOTDATA  : Data from the IOTs"
    echo "         - SGSIO    : SGS I/O data"
    echo ""
    exit $1
}

## Parse command line
while getopts :ht: OPT; do
    case $OPT in
        h ) usage 0;;
        t ) TYPE="$OPTARG"; providedTYPE=true ;;
        \?) echo "Unknown option: -$OPTARG" >&2; usage 1;;
        : ) echo "Missing option argument for -$OPTARG" >&2; usage 1;;
        * ) echo "Unimplemented option: -$OPTARG" >&2; usage 1;;
    esac
done

if ((OPTIND == 1)); then
    echo "No options specified"; usage 0
fi

shift $((OPTIND - 1))
OPTIND=1

if (($# == 0)); then
    echo "No input files specified"; usage 0
fi

ITEMS="$@"
if ! $providedTYPE || [[ ! " ${ValidTypes} " == *" ${TYPE} "* ]]; then
    echo "Invalid type of file(s) to be ingested" >&2 ; usage 1
fi

absFullName () {
    echo "$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")"
}

############################################################

IngestionScript="EASIngestor.sh -d "
MetadataGenerationScript="MetadataGenerator.sh"
### HK/TM Generated products
ingest_HKTM () {
    # ITEMS wil be a zip file with N FITS and 1 XML file
    ${IngestionScript} ${ITEMS}
}

### Ref. Survey, ICRs, SSR-I, SSR/FD
ingest_OPPLAN () {
    # ITEMS wil be a zip file with all the Op.Planning Products
    # We uncompress it and generate the XML metadata object
    ZIPFILE=$(absFullName ${ITEMS})
    CURDIR=$(pwd)
    cd ${TMPDIR}
    unzip ${ZIPFILE}
    FILES=$(find $TMPDIR -type f)
    XML=${TMPDIR}/$(basename ${ITEMS} .zip).xml
    ${MetadataGenerationScript} -t OP_PLAN -o ${XML} ${FILES}
    ${IngestionScript} ${XML} ${FILES}
    cd ${CURDIR}
}

### Operational Sky Survey
ingest_OSS () {
    # ITEMS wil be a gzipped file with the OSS (already a XML object)
    # We uncompress, and ingest it
    OSS=${TMPDIR}/$(basename ${ITEMS} .gz)
    gunzip -c ${ITEMS} > ${OSS}
    ${IngestionScript} ${OSS}
}

### The Mission Data Base set of files
ingest_MIB () {
    # Only 1 file, the MIB package; we need to generate the metadata object
    XML=${TMPDIR}/$(basename ${ITEMS}).xml
    ${MetadataGenerationScript} -t MIB -o ${XML} ${ITEMS}
    ${IngestionScript} ${XML} ${ITEMS}
}

### The Orbit Ephemeris Message file
ingest_OEM () {
    # Only 1 file, the OEM file; we need to generate the metadata object
    XML=${TMPDIR}/$(basename ${ITEMS}).xml
    ${MetadataGenerationScript} -t OEM -o ${XML} ${ITEMS}
    ${IngestionScript} ${XML} ${ITEMS}
}

### An Instrument Commanding Request
ingest_ICR () {
    # Only 1 file, the ICR fil; we need to generate the metadata object
    XML=${TMPDIR}/$(basename ${ITEMS}).xml
    ${MetadataGenerationScript} -t ICR -o ${XML} ${ITEMS}
    ${IngestionScript} ${XML} ${ITEMS}
}

### Raw Science data files (VIS and NISP)
ingest_SCI () {
    # Only 1 file? We assume that, and we need to generate the metadata object
    XML=${TMPDIR}/$(basename ${ITEMS}).xml
    ${MetadataGenerationScript} -t SCI -o ${XML} ${ITEMS}
    ${IngestionScript} ${XML} ${ITEMS}
}

### LE1 VIS/NISP Products (Enhanced)
ingest_LE1 () {
    # ITEMS wil be a zip file with the LE1 product and the XML file
    ${IngestionScript} ${ITEMS}
}

### QLA Reports
ingest_QLA () {
    # Only 1 file, the report, so we need to generate the metadata object
    XML=${TMPDIR}/$(basename ${ITEMS} .json).xml
    ${MetadataGenerationScript} -t QLA -o ${XML} ${ITEMS}
    ${IngestionScript} ${XML} ${ITEMS}
}

### SRV
ingest_SRV () {
    # ITEMS wil be a zip file
    ${IngestionScript} ${ITEMS}
}

### IOTDATA
ingest_IOTDATA () {
    # ITEMS wil be a zip file
    ${IngestionScript} ${ITEMS}
}

### SGSIO
ingest_SGSIO () {
    # ITEMS wil be a zip file
    ${IngestionScript} ${ITEMS}
}

############################################################

## Main switch

mkdir -p ${TMPDIR}

case ${TYPE} in
    HKTM)      # HK/TM Generated products
        ingest_HKTM
        ;;
    OP_PLAN)    # Ref. Survey, ICRs, SSR-I, SSR/FD
        ingest_OPPLAN
        ;;
    OSS)       # Operational Sky Survey
        ingest_OSS
        ;;
    MIB)       # The Mission Data Base set of files
        ingest_MIB
        ;;
    ICR)       # An Instrument Commanding Request
        ingest_ICR
        ;;
    OEM)       # The Orbit Ephemeris Message file
        ingest_OEM
        ;;
    SCI)       # Raw Science data files (VIS and NISP)
        ingest_SCI
        ;;
    LE1)       # LE1 VIS/NISP Products (Enhanced)
        ingest_LE1
        ;;
    SRV)       # EC Reference Survey
        ingest_SRV
        ;;
    IOTDATA)       # IOTDATA
        ingest_IOTDATA
        ;;
    SGSIO)       # SGSIO
        ingest_SGSIO
        ;;
esac

# Clean up and exit

rm -rf ${TMPDIR}
exit 0
