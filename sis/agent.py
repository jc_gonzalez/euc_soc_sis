#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
agent.py

Module with the Agent class

"""
#----------------------------------------------------------------------

# make print & unicode backwards compatible
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys

_filedir_ = os.path.dirname(__file__)
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"
STRING =                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             str

#----------------------------------------------------------------------

from pytools.tools.dirwatcher import define_dir_watcher
from pytools.tools.actions import ActionsLauncher

import time
import json
import queue
import subprocess

from copy import deepcopy
from pprint import pprint

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J C Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2019 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "June 2019"
__maintainer__ = "J C Gonzalez"
#__url__       = ""

#----------------------------------------------------------------------

class Agent:
    """
    Class Agent

    Implements methods to monitor the different folders in the SOC IF
    implementation, and controls the actions to take, according to the
    different action objects.
    """

    # The following object is used as a template for the actions to be
    # executed when a new file appears in the folder where it is
    # placed.
    #
    # The 'type' can be: 'int' (internal), 'cmd', 'bash_script',
    # 'python2' or 'python' (for Pyhton 3)
    #
    # In the 'args' item, the following variables are replaced:
    # - {file_name}: the base file of the new file
    # - {file_path}: the full file name, included the path, of the new file
    # - {src_dir}: the source dir, (the folder where the actions object is located)
    # - {tgt_dir}: the target directory, according to the config. file
    # - {this_dir}: the directory where the actions object file is located
    # - {base_dir}: the SOC IF base dir, according to the configuration
    #
    # The working directory for the execution of the script is the
    # path where the actions object is located.
    #
    # At the initialization, an dummy actions object file is created.
    # Edit it and add blocks to the 'actions' array.

    LoopSleep = 1.0

    def __init__(self, cfg_file, base_path=None):
        self.queues = []
        self.cfg = self.loadConfiguration(file=cfg_file)
        self.cfg['base_path'] = Agent.expandBasePath(self.cfg['base_path'])
        pprint(self.cfg['base_path'])
        pprint(base_path)
        if base_path:
            self.cfg['base_path'] = base_path
        self.baseDir = self.cfg['base_path']
        self.subSys = self.cfg['subsystem']
        self.sisAddress = f'{self.cfg["sis_user"]}@{self.cfg["sis_host"]}'
        self.sisBasePath = self.cfg["sis_base_bath"]
        self.dirWatchers = []
        self.launcher = ActionsLauncher(self.baseDir)

    @staticmethod
    def expandBasePath(bpth):
        """
        Expand in the given path any environment variable in the form @VAR@
        :param bpth: the path provided
        :return: the expanded path, with env.vars. substituted
        """
        return ''.join([x if not x in os.environ else os.environ[x] for x in bpth.split('@')])

    @staticmethod
    def loadConfiguration(file):
        """
        Load configuration file
        :param file: The path of the configuration file name
        :return: -
        """
        try:
            with open(file, 'r') as fcfg:
                cfg = json.load(fcfg)
        except Exception as ee:
            logger.error('Cannot open configuration file {}'.format(file))
            logger.fatal('{}'.format(ee))
            return None

        return cfg

    @staticmethod
    def createIfNotExists(folder):
        """
        Create a nested folder if it does not exist
        :param folder: The folder to create
        """
        if not os.path.exists(folder):
            os.makedirs(folder)
            logger.info('Creating folder {}'.format(folder))
            return True
        else:
            logger.info('Folder {} already exists'.format(folder))
            return False

    @staticmethod
    def idToPath(base, src, io, data):
        """
        Build the relative (if base is None) or the complete path for a
        data flow folder
        :param base: Base path for the entire folder structure
        :param src: Source identifier
        :param io: in/out
        :param data: Data flow identifier
        :return: The built path as a string
        """
        actualId = src.replace('_', '/')
        relPath = os.path.join(actualId, io)
        if data is not None:
            relPath = os.path.join(relPath, data)
        if base is None:
            return relPath
        else:
            return os.path.join(base, relPath)

    def createActions(self, src, tgts, acts):
        """
        Create the actions object file according to the internal and external provided,
        and save it to file
        :param src: The src subsystem
        :param tgts: The target subsystem(s)
        :param acts: Object with lists of internal and external action objects
        :return: True if succeeded, False otherwise
        """
        # Initialize actions object from the template
        actions = deepcopy(ActionsLauncher.ActionsObjectTpl)
        actions['actions'].clear()

        # Internal archive action (make it first)
        if 'archive' in acts:
            actions['actions'].append(ActionsLauncher.ActionInternalSaveLocalArch)
            acts.remove('archive')

        # Parse rest of actions
        for act in acts:
            if isinstance(act, dict):
                actions['actions'].append(act)
            else:
                newAct = deepcopy(ActionsLauncher.InternalActions[act])
                newAct['type'] = 'int'
                newAct['args'] = ';'.join(tgts)
                actions['actions'].append(newAct)

        # Save actions file
        try:
            with open(os.path.join(src, 'actions.json'), 'w') as fact:
                fact.write(json.dumps(actions, indent=4))
            return True
        except:
            return False

    def initMonitoring(self, fromFolder=None, toFolder=None,
                       remoteAddr=None, remoteFolder=None):
        """
        Create a dummy actions object file and launch monitoring
        :param fromFolder: The FROM folder to create
        :param toFolder: The TO folder to create
        :param remoteAddr: Remote machine address
        :param remoteFolder: Remote machine folder
        """
        # Launch monitoring
        q = queue.Queue()
        isRemote = remoteFolder is not None
        dw = None
        if fromFolder is not None:
            dw = define_dir_watcher(fromFolder, q)
            self.createIfNotExists(fromFolder)
        if toFolder is not None:
            self.createIfNotExists(toFolder)
        self.queues.append({'from': fromFolder, 'to': toFolder,
                            'this_folder_is': 'from',
                            'is_remote': isRemote,
                            'address': remoteAddr,
                            'remote_folder': remoteFolder,
                            'current_items': None,
                            'queue': q, 'thread': dw})
        self.dirWatchers.append(dw)

    def initialize(self, cfg : dict):
        """
        Ensure the folders exist
        :param cfg: The configuration dictionary
         :return: -
        """
        logger.info('SIS Agent Running for {}'.format(self.subSys))
        logger.info('Base Path: {}'.format(self.baseDir))
        logger.info('Ensuring the SIS agent folders exist under base path')

        # Get identifiers
        identifiers = dict(cfg['act_id'])

        # Get predefined actions and substitute env. vars.
        predefined_actions = dict(cfg['predefined_actions'])
        for act_id,act in predefined_actions.items():
            act['id'] = act_id
            for k,v in act.items():
                if len(v) < 1: continue
                vvitems = [os.getenv(vv[1:], 'UNKNOWN') if vv[0] == '$' else vv
                           for vv in v.split('/')]
                act[k] = '/'.join(vvitems)
        ActionsLauncher.InternalActions.update(predefined_actions)
        pprint(ActionsLauncher.InternalActions)

        # Create folders for flow transfer, according to configuration file
        for dataId, dataSpec in cfg['data_flows'].items():
            if dataId[0] == '-': continue

            datasetName = dataSpec['name']
            logger.info('- Data set {}'.format(datasetName))

            from_elem = dataSpec['source']
            circSourceDir = self.idToPath(self.baseDir, identifiers[from_elem], 'in', dataId)
            self.createIfNotExists(circSourceDir)

            if not self.createActions(circSourceDir, [], dataSpec["actions"]):
                logger.error(f'Could not create folder actions for {from_elem} => {dataId}')
                continue

            remoteFolder, remoteAddr = None, None
            if len(dataSpec["remote"]) > 0:
                remoteFolder = os.path.join(self.sisBasePath, dataSpec["remote"])
                remoteAddr = self.sisAddress

            self.initMonitoring(fromFolder=circSourceDir,
                                remoteAddr=remoteAddr,
                                remoteFolder=remoteFolder)

        # Create additional, management folders
        for d in ['local_archive']:
            self.createIfNotExists(os.path.join(self.baseDir, d))

    def getRemoteFolderItems(self, folder, address):
        """
        Get the list of files in a remote folder
        :param folder: Remote folder to check
        :param address: Address (user@host) to connect to the remote host
        :return: List of files
        """
        cmd = 'ls -1 {}'.format(folder)
        ssh = subprocess.Popen(['ssh', address, cmd], shell=False,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        result = set()
        for line in ssh.stdout.readlines():
            item = line.decode('ascii')
            result.add(item[:-1] if item[-1] == '\n' else item)

        return result

    def skipControlFiles(self, entry):
        """
        Return true if the file can be skipped
        :param entry: The file name
        :return: True/False
        """
        entry_basename = os.path.basename(entry)
        if entry[-5:] == '.part' or entry_basename == 'actions.json' or \
                entry_basename[0:4] in ('log.', 'err.'):
            return True
        return False

    def synchronizeRemoteAndLocalFolders(self, qentry):
        """
        Check remote folder, and get new items found into local folder
        :param qentry: Paramters for local and remote folders monitoring
        :return: -
        """
        localDir = qentry['from']
        address = qentry['address']
        remoteDir = qentry['remote_folder']
        current_items = qentry['current_items']
        if current_items is None:
            current_items = self.getRemoteFolderItems(remoteDir, address)
        new_items_list = self.getRemoteFolderItems(remoteDir, address)
        new_items = set.difference(new_items_list, current_items)
        logger.debug(f'Syncing...: {current_items} - {new_items_list} => {new_items}')
        for item in set(new_items):
            logger.info(f'>> item = [{item}]')
            # if item[-1] == '\n': item = item[:-1]
            # if item in mqueue['current_items']: continue
            if self.skipControlFiles(item): continue
            newItem = os.path.join(localDir, '.'.join([item, 'part']))
            for cmd in ['scp -qC {}:{}/{} {}'.format(address, remoteDir, item, newItem),
                        'ln {} {}'.format(newItem, os.path.join(localDir, item)),
                        'rm -f {}'.format(newItem)]:
                logger.info(f'>> cmd = {cmd}')
                subprocess.call(cmd.split())
        qentry['current_items'] = new_items_list

    def monitor(self, entry):
        """
        Check all the queues, looking for new entries, and launching the
        appropriate action (according to the actions object file
        :return: -
        """
        # If the entry is for a remote folder monitoring
        if entry['is_remote']:
            self.synchronizeRemoteAndLocalFolders(entry)

        # Else, is for a local folder
        fromFolder = entry['from']
        toFolder = entry['to']
        thisFolder = entry[entry['this_folder_is']]
        q : queue.Queue = entry['queue']
        while not q.empty():
            item = q.get()
            if self.skipControlFiles(item): continue
            logger.info('New file {}'.format(item))

            logger.info('Launching...')
            self.launcher.launchActions(folder=thisFolder, file=item,
                                        src_dir=fromFolder, tgt_dir=toFolder)
            logger.info('done.')

    def runMainLoop(self):
        """
        Run loop of Master
        :return: -
        """
        logger.info('START')
        iteration = 0

        try:
            while True:
                iteration = iteration + 1
                logger.debug('Iteration {}'.format(iteration))

                # Perform check of queues
                for entry in self.queues:
                    self.monitor(entry)

                time.sleep(Agent.LoopSleep)

        except Exception as ee:
            logger.error('{}'.format(ee))
            logger.debug('Iteration {}'.format(iteration))

    def run(self):
        """
        Launch the Agent process
        :return: -
        """
        # Create / List folders
        self.initialize(cfg=self.cfg)

        # Run main loop
        self.runMainLoop()

        # When main loop is finished, wrap up and end program
        self.terminate()

    def terminate(self):
        """
        Wrap-up and finish execution
        :return: -
        """
        logger.info('END')
        for thr in self.dirWatchers:
            thr.join()


def main():
    pass


if __name__ == '__main__':
    main()
